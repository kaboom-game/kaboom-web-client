# Setup the project

- Clone the project on your machine
```
git clone https://gitlab.com/kaboom-game/kaboom-web-client.git kaboom-web-client
```
- Go into the project directory
```
cd !$
```
- Install the dependencies
```
npm install
```

# Commands

## Development

You can develop using hot reload with: 
```
npm run dev
```

## Deployment

An AOT build will be launched with:
```
npm run build
```

You can deploy this build on a Node.js express server using:
```
npm run start
```

As a shortcut, you can run the two previous commands with:
```
npm run build-and-deploy
```

## Tests

You can run all the tests at once using:
```
npm run ci
```

### Linter

```
npm run lint
```

### Unit tests

For hot reloading unit tests with GUI:
```
npm run test
```

For pure CLI:
```
npm run test:headless
```

### E2E tests

For hot reloading E2E tests with GUI:
```
npm run cypress:open
```
> **Warning:** You must have the project running.

For pure CLI:
```
npm run cypress:run
```
> **Warning:** You must have the project running.

Production End-to-End tests:
```
cypress:prod
```

## Analytics

```
npm run build:stats
```
