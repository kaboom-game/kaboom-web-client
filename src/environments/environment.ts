
export const environment = {
  apiUrl: 'http://localhost:8080',
  wsUrl: 'http://localhost:8081',
  production: false,
  debug: false,
  useMock: false,
  socialLogin: {
    googleClientId: '239019224887-kukv12p38e6n7nn84scmpnqtct4367le.apps.googleusercontent.com',
    facebookAppId: ''
  }
};
