import { Component } from '@angular/core';

@Component({
  selector: 'app-minimized',
  templateUrl: './minimized.component.html',
  styleUrls: ['./minimized.component.scss']
})
export class MinimizedComponent {}
