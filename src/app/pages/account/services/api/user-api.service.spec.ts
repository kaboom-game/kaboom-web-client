import { TestBed } from '@angular/core/testing';

import { UserApiService } from './user-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../web-service/web-service.module';

describe('UserApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,

      WebServiceModule
    ],
    providers: [
      UserApiService
    ]
  }));

  it('should be created', () => {
    const service: UserApiService = TestBed.get(UserApiService);
    expect(service).toBeTruthy();
  });
});
