import { TestBed } from '@angular/core/testing';

import { MockUserApiService } from './mock-user-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../web-service/web-service.module';

describe('MockUserApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,

      WebServiceModule
    ],
    providers: [
      MockUserApiService
    ]
  }));

  it('should be created', () => {
    const service: MockUserApiService = TestBed.get(MockUserApiService);
    expect(service).toBeTruthy();
  });
});
