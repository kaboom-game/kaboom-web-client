import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountComponent } from './account.component';
import { MatCardModule, MatIconModule, MatInputModule, MatSelectModule, MatMenuModule,
  MatButtonModule, MatDialogModule, MatProgressBarModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';
import { MockUserApiService } from '../../services/mock/mock-user-api.service';
import { UserApiService } from '../../services/api/user-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../web-service/web-service.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AccountComponent', () => {
  let component: AccountComponent;
  let fixture: ComponentFixture<AccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,

        MatCardModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatMenuModule,
        MatButtonModule,
        MatDialogModule,
        MatProgressBarModule,

        SharedModule,
        WebServiceModule
      ],
      declarations: [
        AccountComponent
      ],
      providers: [
        {
          provide: UserApiService,
          useClass: MockUserApiService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
