import { TestBed } from '@angular/core/testing';

import { ContestApiService } from './contest-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../../web-service/web-service.module';

describe('ContestApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      WebServiceModule
    ],
    providers: [
      ContestApiService
    ]
  }));

  it('should be created', () => {
    const service: ContestApiService = TestBed.get(ContestApiService);
    expect(service).toBeTruthy();
  });
});
