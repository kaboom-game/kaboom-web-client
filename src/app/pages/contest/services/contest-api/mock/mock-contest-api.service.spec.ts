import { TestBed } from '@angular/core/testing';

import { MockContestApiService } from './mock-contest-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../../web-service/web-service.module';

describe('MockContestApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      WebServiceModule
    ],
    providers: [
      MockContestApiService
    ]
  }));

  it('should be created', () => {
    const service: MockContestApiService = TestBed.get(MockContestApiService);
    expect(service).toBeTruthy();
  });
});
