import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateContestComponent } from './create-contest.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule, MatCardModule, MatButtonModule, MatDividerModule, MatRippleModule,
  MatSliderModule, MatSlideToggleModule, MatFormFieldModule, MatInputModule, MatSelectModule,
  MatCheckboxModule, MatDialogModule, MatIconModule, MatListModule } from '@angular/material';
import { WebServiceModule } from '../../../../web-service/web-service.module';
import { SharedModule } from '../../../../shared/shared.module';
import { MockContestApiService } from '../../services/contest-api/mock/mock-contest-api.service';
import { ContestApiService } from '../../services/contest-api/api/contest-api.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CreateContestComponent', () => {
  let component: CreateContestComponent;
  let fixture: ComponentFixture<CreateContestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,

        MatProgressBarModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        MatRippleModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatListModule,

        WebServiceModule,
        SharedModule
      ],
      declarations: [ CreateContestComponent ],
      providers: [
        {
          provide: ContestApiService,
          useClass: MockContestApiService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
