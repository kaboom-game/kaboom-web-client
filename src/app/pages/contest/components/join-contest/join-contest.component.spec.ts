import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinContestComponent } from './join-contest.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule, MatCardModule, MatButtonModule, MatDividerModule, MatIconModule, MatInputModule } from '@angular/material';
import { WebServiceModule } from '../../../../web-service/web-service.module';
import { SharedModule } from '../../../../shared/shared.module';
import { MockContestApiService } from '../../services/contest-api/mock/mock-contest-api.service';
import { ContestApiService } from '../../services/contest-api/api/contest-api.service';

describe('JoinContestComponent', () => {
  let component: JoinContestComponent;
  let fixture: ComponentFixture<JoinContestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,

        MatProgressBarModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,

        WebServiceModule,
        SharedModule
      ],
      declarations: [ JoinContestComponent ],
      providers: [
        {
          provide: ContestApiService,
          useClass: MockContestApiService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
