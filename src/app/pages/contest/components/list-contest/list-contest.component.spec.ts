import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListContestComponent } from './list-contest.component';
import { ContestPreviewComponent } from './contest-preview/contest-preview.component';
import { MatIconModule, MatCardModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MockContestApiService } from '../../services/contest-api/mock/mock-contest-api.service';
import { ContestApiService } from '../../services/contest-api/api/contest-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../../../web-service/web-service.module';

describe('ListContestComponent', () => {
  let component: ListContestComponent;
  let fixture: ComponentFixture<ListContestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        TranslateModule.forRoot(),

        MatIconModule,
        MatCardModule,

        WebServiceModule
      ],
      declarations: [
        ListContestComponent,
        ContestPreviewComponent
      ],
      providers: [
        {
          provide: ContestApiService,
          useClass: MockContestApiService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
