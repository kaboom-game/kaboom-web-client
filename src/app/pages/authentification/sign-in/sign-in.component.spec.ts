import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInComponent } from './sign-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';
import { MatCardModule, MatButtonModule, MatIconModule, MatInputModule, MatMenuModule,
  MatTooltipModule, MatDividerModule, MatProgressSpinnerModule } from '@angular/material';
import { WebServiceModule } from 'src/app/web-service/web-service.module';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthServiceConfigItem } from 'angularx-social-login/src/auth.service';
import { environment } from 'src/environments/environment';

/* Alternatives to basic login. */
const socialLogin: AuthServiceConfigItem[] = [
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.socialLogin.googleClientId)
  }
];

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        SocialLoginModule,

        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatTooltipModule,
        MatDividerModule,
        MatProgressSpinnerModule,

        WebServiceModule,
        SharedModule
      ],
      declarations: [ SignInComponent ],
      providers: [
        {
          provide: AuthServiceConfig,
          useFactory: () => new AuthServiceConfig(socialLogin)
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
