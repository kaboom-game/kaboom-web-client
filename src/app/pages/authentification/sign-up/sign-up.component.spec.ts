import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { MatCardModule, MatButtonModule, MatIconModule, MatInputModule, MatSelectModule,
  MatMenuModule, MatTooltipModule, MatStepperModule, MatDividerModule, MatCheckboxModule,
  MatDialogModule, MatProgressSpinnerModule } from '@angular/material';
import { WebServiceModule } from '../../../web-service/web-service.module';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        MatPasswordStrengthModule.forRoot(),

        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatMenuModule,
        MatTooltipModule,
        MatStepperModule,
        MatDividerModule,
        MatCheckboxModule,
        MatDialogModule,
        MatProgressSpinnerModule,

        WebServiceModule,
        SharedModule
      ],
      declarations: [ SignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
