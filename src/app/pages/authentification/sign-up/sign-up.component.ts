import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

// Validators.
import { CustomValidators } from '../../../shared/form/custom-validators';
import { executeWithInterval } from 'src/app/shared/form/utils';

// Services.
import { FormValidationsService } from '../../../shared/form/services/form-validations.service';
import { TranslationService } from '../../../shared/translation/translation.service';
import { UserSignApiService } from '../../../web-service/user-sign/api/user-sign-api.service';
import { AuthentificationService } from '../../../web-service/authentification/authentification.service';
import { NotificationService } from '../../../shared/notification/notification/notification.service';

// Components.
import { TosValidationComponent } from '../tos-validation/tos-validation.component';

// Models.
import { SignUp } from '../../../web-service/user-sign/models/sign-up.model';

/**
 * SignUp view.
 */
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  /** Language part. */
  languageSection = this.fb.group({
    language: ['fr', [ Validators.required ]]
  });

  /** Personal informations part. */
  personalInformationsSection = this.fb.group({

    /* Username. */
    username: ['', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(50)
    ]],

    /* Email address. */
    emailAddress: ['', [
      Validators.required,
      Validators.email
    ]]
  });

  /** Security part. */
  securitySection = this.fb.group({

    /* First selection. */
    password: ['', [
      Validators.required,
      CustomValidators.password
    ]]
  });

  /** Legal part. */
  legalSection = this.fb.group({

    acceptTOS: [false, [ Validators.required ]]
  });

  /** Says if the password must be displayed. */
  hidePassword = true;

  /** Says if the page is loading. */
  loading = false;

  constructor(private readonly fb: FormBuilder,
              private readonly dialog: MatDialog,
              private readonly translate: TranslationService,
              private readonly webService: UserSignApiService,
              private readonly authentification: AuthentificationService,
              private readonly notification: NotificationService) {}

  /** Opens up the TOS modal. */
  openTOS() {
    this.dialog.open(TosValidationComponent, {
      autoFocus: false,
      width: '500px'
    });
  }

  /**
   * Display error message of the control.
   */
  getErrorMessage(path: string) {
    // return this.validation.getErrorMessage(this.form, path);
  }

  /** Called when the set of forms is submitted. */
  submit() {

    if (this.languageSection.valid && this.personalInformationsSection.valid &&
        this.securitySection.valid && this.legalSection.valid) {

      this.loading = true;

      // Build the DTO.
      const dto = new SignUp();

      dto.language = this.languageSection.get('language').value;
      dto.username = this.personalInformationsSection.get('username').value;
      dto.email = this.personalInformationsSection.get('emailAddress').value;
      dto.password = this.securitySection.get('password').value;

      const signUp$ = this.webService.signUp(dto);

      executeWithInterval(signUp$, 1500).subscribe(token => {

        this.notification.success('NOTIFICATION.SIGN_UP.SUCCESS');
        this.authentification.login(token);
      }, () => {
        // TODO: Handle unexpected errors.
      });
    }
  }

  ngOnInit() {
    this.languageSection.get('language').valueChanges.subscribe(val => this.translate.setLanguage(val));
  }
}
