import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

// Material dependencies.
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// Additional material dependencies for password input.
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';

// Social login.
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthServiceConfigItem } from 'angularx-social-login/src/auth.service';

// Components.
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TosValidationComponent } from './tos-validation/tos-validation.component';

// Modules.
import { SharedModule } from '../../shared/shared.module';
import { WebServiceModule } from '../../web-service/web-service.module';

import { environment } from 'src/environments/environment';

/* Alternatives to basic login. */
const socialLogin: AuthServiceConfigItem[] = [
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.socialLogin.googleClientId)
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule,
    MatPasswordStrengthModule.forRoot(),
    SocialLoginModule,

    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatMenuModule,
    MatTooltipModule,
    MatStepperModule,
    MatDividerModule,
    MatCheckboxModule,
    MatDialogModule,
    MatProgressSpinnerModule,

    WebServiceModule,
    SharedModule,
  ],
  declarations: [
    SignInComponent,
    SignUpComponent,
    TosValidationComponent
  ],
  entryComponents: [
    TosValidationComponent
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: () => new AuthServiceConfig(socialLogin)
    }
  ]
})
export class AuthentificationModule {}
