import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TosValidationComponent } from './tos-validation.component';

describe('TosValidationComponent', () => {
  let component: TosValidationComponent;
  let fixture: ComponentFixture<TosValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TosValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TosValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
