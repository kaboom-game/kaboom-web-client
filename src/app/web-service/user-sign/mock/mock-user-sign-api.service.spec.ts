import { TestBed } from '@angular/core/testing';

import { MockUserSignApiService } from './mock-user-sign-api.service';
import { WebServiceModule } from '../../web-service.module';
import { HttpClientModule } from '@angular/common/http';

describe('MockUserSignApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      WebServiceModule
    ],
    providers: [
      MockUserSignApiService
    ]
  }));

  it('should be created', () => {
    const service: MockUserSignApiService = TestBed.get(MockUserSignApiService);
    expect(service).toBeTruthy();
  });
});
