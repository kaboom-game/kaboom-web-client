import { TestBed } from '@angular/core/testing';

import { UserSignApiService } from './user-sign-api.service';
import { HttpClientModule } from '@angular/common/http';
import { WebServiceModule } from '../../web-service.module';

describe('UserSignApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      WebServiceModule
    ],
    providers: [
      UserSignApiService
    ]
  }));

  it('should be created', () => {
    const service: UserSignApiService = TestBed.get(UserSignApiService);
    expect(service).toBeTruthy();
  });
});
