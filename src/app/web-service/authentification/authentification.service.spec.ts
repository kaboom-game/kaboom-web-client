import { TestBed } from '@angular/core/testing';

import { AuthentificationService } from './authentification.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthentificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      AuthentificationService
    ]
  }));

  it('should be created', () => {
    const service: AuthentificationService = TestBed.get(AuthentificationService);
    expect(service).toBeTruthy();
  });
});
