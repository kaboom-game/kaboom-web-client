import { TestBed } from '@angular/core/testing';

import { TranslationService } from './translation.service';
import { TranslateModule } from '@ngx-translate/core';

describe('TranslationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      TranslateModule.forRoot()
    ],
    providers: [
      TranslationService
    ]
  }));

  it('should be created', () => {
    const service: TranslationService = TestBed.get(TranslationService);
    expect(service).toBeTruthy();
  });
});
