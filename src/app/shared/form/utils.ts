import { combineLatest, Observable, timer } from 'rxjs';
import { map } from 'rxjs/operators';

export const executeWithInterval = <T>(obs: Observable<T>, interval: number): Observable<T> => combineLatest([timer(interval), obs])
  .pipe(map(([_, data]) => data));
