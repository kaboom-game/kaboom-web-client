import { environment } from './environments/environment';

export function mockService(service, mockedService) {
  return environment.useMock ? mockedService : service;
}
