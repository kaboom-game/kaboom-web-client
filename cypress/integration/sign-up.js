describe('Sign-Up page', function() {

  before(() => cy.visit('/sign-up'))

  const switchLanguage = value => {
    cy.get('mat-select').click()
    cy.get(`mat-option[value=${value}]`).click()
  }

  context('Language switch', () => {

    const assertSectionLabel = (index, text) => cy.get(`#cdk-step-label-1-${index - 1} > .mat-step-label`)
      .should('contain', text)

    it('should switch to english', () => {

      switchLanguage('en')
  
      // Check card title.
      cy.get('mat-card-title').contains('Sign-Up')
  
      // Test: Language section.
      assertSectionLabel(1, 'Spoken language')
  
      // Test: Language section.
      assertSectionLabel(2, 'Personal informations')
  
      // Test: Security section.
      assertSectionLabel(3, 'Security')
  
      // Test: Confirmation section.
      assertSectionLabel(4, 'Confirm sign-up')
    })
  
    it('should switch to french', () => {
  
      switchLanguage('fr')
  
      // Check card title.
      cy.get('mat-card-title').contains('Inscription')
  
      // Test: Language section.
      assertSectionLabel(1, 'Langage souhaité')
  
      // Test: Language section.
      assertSectionLabel(2, 'Informations personnelles')
  
      // Test: Security section.
      assertSectionLabel(3, 'Sécuriser le compte')
  
      // Test: Confirmation section.
      assertSectionLabel(4, 'Terminer l\'inscription')
    })
  })

  context('Successful form filling', () => {

    // Next button.
    const next = () => cy.get('.mat-vertical-stepper-content[aria-expanded=true]').contains('Next');

    it('should fill the language section', () => {
      cy.visit('/sign-up')

      switchLanguage('en')

      next().click();
    })

    it('should fill the personal informations section', () => {

      next().should('be.disabled')

      cy.get('input[formcontrolname=username]').type('Ratata')
      cy.get('input[formcontrolname=emailAddress]').type('ratata.pokemon@gmail.com')

      next().should('not.be.disabled').then(e => e.click())
    })

    it('should fill the security section', () => {

      const password = () => cy.get('input[formcontrolname=password]')

      next().should('be.disabled')

      password().type('P@ssword')
      password().should('have.attr', 'type', 'password');

      // Toggle password visibility.
      cy.get('.mat-vertical-stepper-content[aria-expanded=true] mat-form-field button').click()

      password().should('have.attr', 'type', 'text')

      next().should('not.be.disabled').then(e => e.click())
    })

    it('should confirm sign-up', () => {

      cy.server()

      cy.route({
        method: 'POST',
        url: `http://localhost:8080/user`,
        response: {
          token: 'token'
        }
      })

      const signUp = () => cy.get('.mat-vertical-stepper-content[aria-expanded=true]').contains('Sign-Up')

      signUp().should('be.disabled')

      // Test TOS
      cy.get('.tos').click()
      cy.get('.mat-dialog-container').should('be.visible')      
      cy.get('app-tos-validation button').click();
      cy.get('.mat-dialog-container').should('not.be.visible')

      cy.get('.mat-checkbox-input').check({ force: true })

      signUp().should('not.be.disabled').then(e => e.click())
    })
  })

})
